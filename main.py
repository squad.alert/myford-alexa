import requests
import os
import json
import time
import myford
import boto3

from base64 import b64decode

PASSWORD = os.environ['PASSWORD']
if len(PASSWORD) > 64:
    PASSWORD = boto3.client('kms').decrypt(CiphertextBlob=b64decode(PASSWORD))['Plaintext']
USERNAME = os.environ['USERNAME']
applicationId = os.environ['APP_ID']

mf = myford.MyFord(USERNAME,PASSWORD)

def lambda_handler(event, context):

    if event['session']['application']['applicationId'] != applicationId:
        print "Invalid Application ID"
        raise
    else:
        # Not using sessions for now
        sessionAttributes = {}
        if mf.login() == False:
            print "Invalid login"
            return buildSpeechletResponse("Invalid login", "Error, invalid login", "Check your credentials and try again", True)

        print "Login OK, session_id:"+mf.session_id
        if event['session']['new']:
            onSessionStarted(event['request']['requestId'], event['session'])
        if event['request']['type'] == "LaunchRequest":
            speechlet = onLaunch(event['request'], event['session'])
            response = buildResponse(sessionAttributes, speechlet)
        elif event['request']['type'] == "IntentRequest":
            speechlet = onIntent(event['request'], event['session'])
            response = buildResponse(sessionAttributes, speechlet)
        elif event['request']['type'] == "SessionEndedRequest":
            speechlet = onSessionEnded(event['request'], event['session'])
            response = buildResponse(sessionAttributes, speechlet)

        # Return a response for speech output
        return(response)



# Called when the session starts
def onSessionStarted(requestId, session):
    print("onSessionStarted requestId=" + requestId + ", sessionId=" + session['sessionId'])

# Called when the user launches the skill without specifying what they want.
def onLaunch(launchRequest, session):
    # Dispatch to your skill's launch.
    getWelcomeResponse()

# Called when the user specifies an intent for this skill.
def onIntent(intentRequest, session):
    intent = intentRequest['intent']
    intentName = intentRequest['intent']['name']

    # Dispatch to your skill's intent handlers
    if intentName == "StateIntent":
        return stateResponse(intent)
    elif intentName == "MoveIntent":
        return moveIntent(intent)
    elif intentName == "HelpIntent":
        return getWelcomeResponse()
    else:
        print "Invalid Intent (" + intentName + ")"
        raise

# Called when the user ends the session.
# Is not called when the skill returns shouldEndSession=true.
def onSessionEnded(sessionEndedRequest, session):
    # Add cleanup logic here
    print "Session ended"

def getWelcomeResponse():
    cardTitle = "Welcome"
    speechOutput = """You can remote start your car by saying, ask my ford to start."""

    # If the user either does not reply to the welcome message or says something that is not
    # understood, they will be prompted again with this text.
    repromptText = 'Ask me to remote start your car by saying ask my ford to start'
    shouldEndSession = True

    return (buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession))

def moveIntent(intent):
    """
    Ask my ford to {unlock|lock|start|stop}
        "intent": {
          "name": "StateIntent",
          "slots": {
            "carstate": {
              "name": "carstate",
              "value": "start"
            }
          }
        }
    """
    command_id = ""
    action = ""
    doing = ""
    ok = True if 'value' in intent['slots']['carstate'] else False
    if ok:
        action = intent['slots']['carstate']['value']
        doing = action
        
        if (action in ["close", "closed", "closing", "lock", "locked", "locking"]):
            command_id = mf.close()
            doing = "locking"
        elif (action in ["open", "opened", "opening", "unlock", "unlocked", "unlocking"]):
            command_id = mf.open()
            doing = "unlocking"
        elif (action in ["start", "started", "starting"]):
            command_id = mf.start()
            doing = "starting"
        elif (action in ["stop", "stopped", "stopping"]):
            command_id = mf.stop()
            doing = "stopping"
        else:
            ok = False
    
    if not ok:
        speechOutput = "I didn't understand that. You can say ask the car to lock, unlock or start"
        cardTitle = "Try again"
    elif command_id == "":
        speechOutput = "Hmm, I couldn't "+action+" your car."
        cardTitle = "Something wrong"
    else:
        speechOutput = "Ok, I'm "+doing+" your car"
        cardTitle = doing.capitalize()+" your car"

    repromptText = "I didn't understand that. You can say ask the car if it's open, or tell it to lock or start"
    shouldEndSession = True

    return(buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession))

def stateResponse(intent):
    """
    Ask my car if it's {locked|unlocked|starting}
        "intent": {
          "name": "StateIntent",
          "slots": {
            "carstate": {
              "name": "carstate",
              "value": "locked"
            }
          }
        }
    """
    carstate = mf.status()

    if 'value' not in intent['slots']['carstate']:
        speechOutput = "Your car is " + carstate
        cardTitle = speechOutput
    elif (intent['slots']['carstate']['value'] in ["open", "opened", "opening", "unlock", "unlocked", "unlocking"]):
        if carstate == "open":
            speechOutput = "Yes, your car is open"
            cardTitle = "Yes, your car is open"
        elif carstate == "closed":
            speechOutput = "No, your car is closed"
            cardTitle = "No, your car is closed"
        else:
            speechOutput = "Your car is " + carstate
            cardTitle = "Your car is " + carstate

    elif (intent['slots']['carstate']['value'] in ["close", "closed", "closing", "lock", "locked", "locking"]):
        if carstate == "closed":
            speechOutput = "Yes, your car is closed"
            cardTitle = "Yes, your car is closed"
        elif carstate == "open":
            speechOutput = "No, your car is open"
            cardTitle = "No, your car is open"
        else:
            speechOutput = "Your car is " + carstate
            cardTitle = "Your car is " + carstate

    else:
        speechOutput = "I didn't understand that. You can say ask the car if it's open"
        cardTitle = "Try again"

    repromptText = "I didn't understand that. You can say ask the car if it's open"
    shouldEndSession = True

    return(buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession))


# --------------- Helpers that build all of the responses -----------------------
def buildSpeechletResponse(title, output, repromptText, shouldEndSession):
    return ({
        "outputSpeech": {
            "type": "PlainText",
            "text": output
        },
        "card": {
            "type": "Simple",
            "title": "MyFord - " + title,
            "content": "MyFord - " + output
        },
        "reprompt": {
            "outputSpeech": {
                "type": "PlainText",
                "text": repromptText
            }
        },
        "shouldEndSession": shouldEndSession
    })

def buildResponse(sessionAttributes, speechletResponse):
    return ({
        "version": "1.0",
        "sessionAttributes": sessionAttributes,
        "response": speechletResponse
    })
