# Alexa-MyFord
Using the Alexa Skills Kit to control your car registered in [MyFord Mobile](https://www.myfordmobile.com).

## Description
By using the Alexa Skills Kit and AWS Lambda, you can control your car through your Amazon Echo.

## Usage
Detailed instruction on how to set up this skill is located in StepByStepInstructions file in this repo.

### Alexa Skills Kit Documentation
The documentation for the Alexa Skills Kit is available on the [Amazon Apps and Services Developer Portal](https://developer.amazon.com/appsandservices/solutions/alexa/alexa-skills-kit/).

### Resources
Here are a few direct links to Alexa and Lambda documentation:

- [Using the Alexa Skills Kit Samples (Node.js)](https://github.com/amzn/alexa-skills-kit-js)
- [Getting Started](https://developer.amazon.com/appsandservices/solutions/alexa/alexa-skills-kit/getting-started-guide)
- [Invocation Name Guidelines](https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/choosing-the-invocation-name-for-an-alexa-skill)
- [Developing an Alexa Skill as an AWS Lambda Function](https://developer.amazon.com/appsandservices/solutions/alexa/alexa-skills-kit/docs/developing-an-alexa-skill-as-a-lambda-function)

### Callouts

Special thanks to jbnunn implementation of [Alexa-MyQGarage](https://github.com/jbnunn/Alexa-MyQGarage) that inspired creation of this skill.

### Disclaimer

The code here is based off of sniffed packets sent from [MyFord Mobile](https://www.myfordmobile.com) website and is subject to change without notice. The authors claim no responsibility for damages to your car or property by use of the code within.
