import requests
import os
import json
import time

class MyFord:

    appVersion = "web-2.0.51"
    HOST_URI = "www.myfordmobile.com"
    LOGIN_ENDPOINT = "services/webLoginPS"
    ADD_COMMAND_ENDPOINT = "services/webAddCommandPS"

    username = "<MYFORD_LOGIN_USERNAME>"
    password = "<MYFORD_LOGIN_PASSWORD>"

    session_id = ""

    def __init__(self, username, password):
        self.username = username
        self.password = password
    
    def open(self):
        return self.change_car_state("UNLOCK_CMD")
    
    def close(self):
        return self.change_car_state("LOCK_CMD")
    
    def start(self):
        return self.change_car_state("START_CMD")
    
    def stop(self):
        return self.change_car_state("CANCEL_START_CMD")
    
    def status(self):
        return "ok" if self.session_id != "" else "unknown"
    
    def login(self):
        self.session_id = ""
        params = { "PARAMS":{
            'emailaddress': self.username,
            'password': self.password,
            "persistent":"1",
            "appVersion":self.appVersion,
            "apiLevel":"1"
        }}
        login = requests.post(
                'https://{host_uri}/{login_endpoint}'.format(
                    host_uri=self.HOST_URI,
                    login_endpoint=self.LOGIN_ENDPOINT),
                    json=params,
                    verify=False
            )
        if login.status_code == 200 :
            auth = login.json()
            if "response" in auth and 'authToken' in auth["response"]:
                self.session_id = auth["response"]['authToken']
        return self.session_id != ""
    
    def change_car_state(self, command):
        if self.session_id == "":
            return ""
            
        payload = {
            "PARAMS":{
                "SESSIONID":self.session_id,
                "LOOKUPCODE":command,
                "apiLevel":"1"
            }
        }
        car_action = requests.post(
            'https://{host_uri}/{device_set_endpoint}'.format(
                host_uri=self.HOST_URI,
                device_set_endpoint=self.ADD_COMMAND_ENDPOINT),
                json=payload,
                verify=False
        )
        command_id = ""
        if car_action.status_code == 200:
            resp = car_action.json() 
            if resp and "COMMANDID" in resp:
                command_id = resp["COMMANDID"];
        return command_id
